require 'rabbitmq_service_util'
require 'bunny'

host = ENV["RABBIT_MQ_HOST"]
port = ENV["RABBIT_MQ_PORT"]
vhost = ENV["RABBIT_MQ_V_HOST"]
user = ENV["RABBIT_MQ_USER"]
pass = ENV["RABBIT_MQ_PASS"]

RabbitMQ = Bunny.new(
  host:  host,
  port:  port,
  vhost: vhost,
  user:  user,
  pass:  pass
)

RabbitMQ.start

