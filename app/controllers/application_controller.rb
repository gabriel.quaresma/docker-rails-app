require 'rabbitmq_service_util'
require 'bunny'

class ApplicationController < ActionController::Base

  def index
    @reponse = health_status
    render json: @reponse
  end

  private

  def health_status
    {
      :database => check_db,
      :redis => check_redis,
      :rabbitmq => check_rabbitmq
    }
  end

  def check_db
    results = ActiveRecord::Base.connection.execute('select * from user')

    if results.present?
      return true
    end

    return false
  end

  def check_redis
    result = REDIS.ping

    return result == 'PONG'
  end

  def check_rabbitmq
    channel = RabbitMQ.create_channel
    temporary_queue = channel.queue("temporary.queue", :exclusive => false, :auto_delete => true)

    result = RabbitMQ.queue_exists?("temporary.queue")
    pp result
    return result
  end

end
